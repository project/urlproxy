
     
      URL Proxy
      Provided by http://vision-media.ca
      Developed by Tj Holowaychuk
     
      ------------------------------------------------------------------------------- 
      INSTALLATION
      ------------------------------------------------------------------------------- 
      
      Simple enable the module. 
      
      This module WILL work with the normal page cache mode, since the user is 
      simply redirected, Drupal will never have a chance to cache the 'proxy page'.
    
      ------------------------------------------------------------------------------- 
      PERMISSIONS
      ------------------------------------------------------------------------------- 
      
      bypass url proxy 
        - allows users to bypass urlproxy_url(), however this does NOT
          work for bypassing urlproxy_redirect().
         
      administer url proxy 
        - allows configuration of URL Proxy settings.
        
      access url proxy reports 
        - allows viewing of various reports such as top hits.  
        
      ------------------------------------------------------------------------------- 
      PUBLIC API
      -------------------------------------------------------------------------------
      
      hook_urlproxy() 
        - invoked by urlproxy_redirect() allows developers to generate
          their own statistics regarding outgoing traffic.
      
      - urlproxy_url()
      - urlproxy_redirect()
      - urlproxy_add_hit()
      - urlproxy_get_hit()
              
      ------------------------------------------------------------------------------- 
      FUTURE GOALS
      ------------------------------------------------------------------------------- 
      
      If you wish to see further development of this module beyond the scope that I 
      have previously stated on this modules project page please contact us at 
      htt://vision-media.ca and we would be more than happy to assist you.
          


