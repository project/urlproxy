<?php

/**
 * @file 
 * Report on core proxy data.
 * @author Tj Holowaychuk <tj@vision-media.ca>
 * @link http://vision-media.ca
 * @package urlproxy
 */

/**
 * Get hits by URL.
 * 
 * @todo theme func
 * 
 * @return string
 *   Markup.
 */
function urlproxy_report_urls() {
  $output = ' ';
  $rows = array();   
  $header = array(t('URL'), t('Hits'));
  $hit_rows = urlproxy_get_hits(); 
                            
  foreach((array) $hit_rows AS $hit){
    $rows[] = array(
        $hit->url,
        $hit->hits,
      );                              
  }               
            
  $output .= theme('table', $header, $rows);
                
  return $output;
}

/**
 * Get hits by hostname.
 * 
 * @todo theme func
 * 
 * @return string
 *   Markup.
 */
function urlproxy_report_hostnames() {
  $output = ' ';
  $rows = array();   
  $hostnames = array();
  $header = array(t('Hostname'), t('Hits'));
  $hit_rows = urlproxy_get_hits();
  
  foreach((array) $hit_rows AS $hit){
    $info = parse_url($hit->url);
    $hostnames[$info['host']][] = $hit->hits;  
  }
                            
  foreach((array) $hostnames AS $hostname => $hits){
    $rows[] = array(
        $hostname,
        array_sum($hits),
      );                             
  }               
            
  $output .= theme('table', $header, $rows);
                           
  return $output;
}

/**
 * Get referers and their hits.
 * 
 * @todo theme func
 * 
 * @return string
 *   Markup.
 */
function urlproxy_report_referers() {
  $output = ' ';
  $rows = array();   
  $referers = array();
  $header = array(t('Referer Path'), t('Hits'));
  $hit_rows = urlproxy_get_hits(NULL, 'referer'); 
      
  foreach((array) $hit_rows AS $referer => $hits){
    foreach((array) $hits AS $hit){
      $referers[$referer][] = $hit->hits;
    }
  }
                            
  foreach((array) $referers AS $referer => $hits){
    $rows[] = array(
        $referer,
        array_sum($hits),
      );                              
  }               
            
  $output .= theme('table', $header, $rows);
                           
  return $output;
}

/**
 * @todo theme
 */
function urlproxy_report_top_hits($hits) {
  $rows = array();   
         
  foreach((array) $hits AS $hit){
    $rows[] = array(
        $hit->url,
        $hit->hits,
      );                              
  }               
            
  $output .= theme('table', array(), $rows);
                
  return $output;
}

/**
 * Return the top hits.
 * 
 * @param array $hits
 * 
 * @param int $limit
 * 
 * @return array
 */
function urlproxy_report_get_top_hits($hits, $limit = 5) {
   $output = array();
   $hits = urlproxy_report_get_grouped_hits($hits, 'hits');
   krsort($hits);
   
   // Since we are working with nested hits 
   // from urlproxy_report_group_hits() we need
   // to iterate them until we populate the requested $limit
   $c = 0;
   while($c != $limit){
     $_hits = array_shift($hits);
     
     foreach((array) $_hits AS $_hit){
       if (count($output) != $limit){
         $output[] = $_hit;
       }
     }
     
     $c++;
   }
          
   return $output;
}

/**
 * Re-group hits.
 * 
 * @param array $hits
 * 
 * @param string $key
 * 
 * @return array
 */
function urlproxy_report_get_grouped_hits($hits, $key) {
  $output = array();
  
  foreach((array) $hits AS $hit){
    $output[$hit->$key][] = $hit;
  }
  
  return $output;  
}

